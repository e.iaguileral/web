<?php
require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM songs");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);
$song = $_POST["song"];
$error = false;
for($i=0;$i<sizeof($result);$i++) {
    if($song == $result[$i]["Nom"]){
        $error = true;
    }
}

if($error){
    $json = array("estat"=>"KO","error"=>"Ya existeix la cançó","canço"=>$song);
    echo json_encode($json);
}else{
        $json = array("estat"=>"OK","error"=>"","canço"=>$song);
        $query = $conn->prepare("INSERT INTO songs (Nom,Reproduccions,Recaptacio) values(:song,0,0)");
        $query->bindParam("song",$song,PDO::PARAM_STR);
        $query->execute();
        echo json_encode($json);
}
?>
