$("#reproduccio").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"nova.php",
        dataType:"json",
        success:function (data){
            console.log(data);
            $("p#text").html(data["missatge"]);
            window.stop();
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
$("#buscar").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"cançons.php",
        data:{"song":$("#song").val()},
        dataType:"json",
        success:function (data){
            console.log(data);
            var tabla = '<table>';
            tabla += '<tr>' +
                '<th align="center">idSongs</th>' +
                '<th align="center">Nom</th>' +
                '<th align="center">Reproduccions</th>' +
                '<th align="center">Recaptacio</th>' +
                '</tr>';
            for(var i = 0; i<data.length;i++){
                tabla+='<tr>';
                tabla+='<td align="center">'+data[i]["id"]+'</td>'+
                '<td>'+data[i]["nom"]+'</td>'+
                '<td align="center">'+data[i]["rep"]+'</td>'+
                '<td align="center">'+data[i]["rec"]+'</td>';
                tabla+='</tr>'
            }
            tabla += '</table>';
            $("p#text").html(tabla);
            window.stop();
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
$("#register").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"registerBD.php",
        data:{"usuari":$("#correure").val(),"pass":$("#passwordre").val()},
        dataType:"json",
        success:function (data){
            if(data["estat"] === "KO"){
                $("p#text").html(data["error"]+" "+data["usuari_app"]).css('color','red');
            }else if(data["estat"] === "OK"){
                $("p#text").html(data["usuari_app"]).css('color','green');
            }
            console.log(data);
            window.stop();
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
$("#login").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"loginBD.php",
        data:{"usuari":$("#correulog").val(),"pass":$("#passwordlog").val()},
        dataType:"json",
        success:function (data){
            console.log(data);
            if(data["estat"] === "KO"){
                $("p#text").html(data["error"]+" "+data["usuari_app"]).css('color','red');
            }else if(data["estat"] === "OK"){
                sessionStorage.setItem("usuarilog",data["usuari_app"]);
                $("p#text").html(data["usuari_app"]).css('color','green');
                window.location.href = "Pàgina_Principal.html";
            }
            console.log(data);
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
$("#afegir").click(function(){
    event.preventDefault();
    $.ajax({
        method:"POST",
        url:"afegircanço.php",
        data:{"song":$("#song").val()},
        dataType:"json",
        success:function (data){
            if(data["estat"] === "KO"){
                $("p#text").html(data["error"]).css('color','red');
            }else if(data["estat"] === "OK"){
                $("p#text").html("S'ha afegit "+data["canço"]).css('color','green');
            }
            console.log(data);
            window.stop();
        },
        error:function (jqXHR, textStatus, error){
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);

        }
    })
});
