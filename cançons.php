<?php
require_once("config.php");
$song = $_POST["song"];
if($song !== ''){
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM songs WHERE Nom like :patata");
    $query->bindParam('patata',$song,PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $json = array(array("id"=>$result[0]["idSongs"],"nom"=>$result[0]["Nom"],"rep"=>$result[0]["Reproduccions"],"rec"=>$result[0]["Recaptacio"]));
    echo json_encode($json);
}else{
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM songs order by Recaptacio desc,Nom asc,Reproduccions desc");
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    $json = array();
    for ($i = 0;$i<sizeof($result);$i++){
        array_push($json,array("id"=>$result[$i]["idSongs"],"nom"=>$result[$i]["Nom"],"rep"=>$result[$i]["Reproduccions"],"rec"=>$result[$i]["Recaptacio"]));
    }
    echo json_encode($json);
}
?>



