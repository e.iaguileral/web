<?php
require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM usuaris_app");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);

$usuari = $_POST["usuari"];
$contrasenya = $_POST["pass"];
$ok = false;
$esta = false;
for($i=0;$i<sizeof($result);$i++) {
    if ($result[$i]["nom"] == $usuari and $contrasenya == $result[$i]["password"]) {
        $ok = true;
    }else if($result[$i]["nom"] == $usuari){
        $esta = true;
    }
}

if($ok){
    $json = array("estat"=>"OK","error"=>"","usuari_app"=>$usuari);
    echo json_encode($json);
}else{
    if(!$esta){
        $json = array("estat"=>"KO","error"=>"Usuari no existeix","usuari_app"=>$usuari);
        echo json_encode($json);
    }else{
        $json = array("estat"=>"KO","error"=>"Credencial incorrecte","usuari_app"=>$usuari);
        echo json_encode($json);
    }
}
?>