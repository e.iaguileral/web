<?php
require_once("config.php");
$conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
$query = $conn->prepare("SELECT * FROM usuaris_app");
$query->execute();
$result = $query->fetchAll(PDO::FETCH_ASSOC);
$usuari = $_POST["usuari"];
$pass = $_POST["pass"];
$error = false;
for($i=0;$i<sizeof($result);$i++) {
    if($usuari == $result[$i]["nom"]){
        $error = true;
    }
}

if($error){
    $json = array("estat"=>"KO","error"=>"usuari existent","usuari_app"=>$usuari);
    echo json_encode($json);
}else{
    if(strpos($usuari,"@ies-sabadell.cat") == false){
        $json = array("estat"=>"KO","error"=>"correu incorrecte","usuari_app"=>$usuari);
        echo json_encode($json);
    }else{
        $json = array("estat"=>"OK","error"=>"","usuari_app"=>$usuari);
        $query = $conn->prepare("INSERT INTO usuaris_app (nom,password) values(:nom,:password)");
        $query->bindParam("nom",$usuari,PDO::PARAM_STR);
        $query->bindParam("password",$pass,PDO::PARAM_STR);
        $query->execute();
        echo json_encode($json);
    }
}
